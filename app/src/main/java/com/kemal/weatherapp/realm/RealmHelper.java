package com.kemal.weatherapp.realm;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by kemal on 20/08/16.
 */
public class RealmHelper {

    public static Realm getRealm(Context context){

        RealmConfiguration config = new RealmConfiguration.Builder(context)
                .name("weatherapp_v1")
                .schemaVersion(1)
                .build();

        Realm realm = Realm.getInstance(config);


        return realm;
    }

}
