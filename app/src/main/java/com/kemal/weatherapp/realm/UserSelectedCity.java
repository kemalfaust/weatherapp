package com.kemal.weatherapp.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by kemal on 20/08/16.
 */
public class UserSelectedCity extends RealmObject {

    @PrimaryKey
    private String cityQueryName;

    private String cityName;

    public UserSelectedCity() {

    }

    public String getCityQueryName() {
        return cityQueryName;
    }

    public void setCityQueryName(String cityQueryName) {
        this.cityQueryName = cityQueryName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
