package com.kemal.weatherapp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.kemal.weatherapp.activities.CityWeatherFragment;
import com.kemal.weatherapp.interfaces.OnWeatherItemDeleteListener;
import com.kemal.weatherapp.models.common.SlidingTabCityModel;

import java.util.ArrayList;

/**
 * Created by kemal on 19/08/16.
 */

public class SlidingTabPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<SlidingTabCityModel> arrayList;
    private OnWeatherItemDeleteListener onWeatherItemDeleteListener;


    public SlidingTabPagerAdapter(FragmentManager fm) {
        super(fm);
        // TODO Auto-generated constructor stub
    }

    public SlidingTabPagerAdapter(FragmentManager fm,ArrayList<SlidingTabCityModel> arrayList,OnWeatherItemDeleteListener onWeatherItemDeleteListener) {
        super(fm);
        this.arrayList = arrayList;
        this.onWeatherItemDeleteListener = onWeatherItemDeleteListener;
    }

    @Override
    public Fragment getItem(int pos) {

        SlidingTabCityModel tabModel = arrayList.get(pos);

        CityWeatherFragment fragment = CityWeatherFragment.newInstance(tabModel, this.onWeatherItemDeleteListener);
        return fragment;
    }

    public SlidingTabCityModel getSlidingTabModel(int pos) {
        return arrayList.get(pos);
    }

    @Override
    public int getCount() {

        if(arrayList != null)
            return arrayList.size();

        return 0;
    }


    @Override
    public CharSequence getPageTitle(int position) {

        if(arrayList != null){

            SlidingTabCityModel model = arrayList.get(position);
            return model.toString();
        }
        return super.getPageTitle(position);
    }




}
