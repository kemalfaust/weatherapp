package com.kemal.weatherapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kemal.weatherapp.R;
import com.kemal.weatherapp.models.weather.Weather;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kemal on 20/08/16.
 */
public class GridAdapterWeatherDays extends BaseAdapter{

    private ArrayList<Weather> arrayList;
    private Context context;

    public GridAdapterWeatherDays(ArrayList<Weather> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public int getCount() {
        if (arrayList == null)
            return 0;
        return arrayList.size();
    }

    @Override
    public Weather getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder = null;

        if (convertView == null || convertView.getTag() == null){

            convertView = LayoutInflater.from(this.context).inflate(R.layout.weather_grid_adapter_item, parent, false);

            holder = new Holder();
            holder.txtDayName = (TextView) convertView.findViewById(R.id.weather_grid_adapter_item_dayName);
            holder.txtMinTemp = (TextView) convertView.findViewById(R.id.weather_grid_adapter_item_minTemp);
            holder.txtMaxTemp = (TextView) convertView.findViewById(R.id.weather_grid_adapter_item_maxTemp);

            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();
        }

        Weather item = getItem(position);

        holder.txtDayName.setText(item.getDate());
        holder.txtMinTemp.setText("min:"+item.getMintempC()+" C");
        holder.txtMaxTemp.setText("max:"+item.getMaxtempC()+" C");

        return convertView;
    }


    static class Holder {
        TextView txtDayName;
        TextView txtMinTemp;
        TextView txtMaxTemp;

    }
}
