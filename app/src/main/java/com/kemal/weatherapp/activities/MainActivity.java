package com.kemal.weatherapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.astuetz.PagerSlidingTabStrip;
import com.kemal.framework.base.BaseAppCompatActivity;
import com.kemal.framework.network.NetworkBaseResponse;
import com.kemal.framework.network.enums.NetworkResponseErrorType;
import com.kemal.framework.network.interfaces.OnNetworkResponseListener;
import com.kemal.framework.volley.VolleyManager;
import com.kemal.weatherapp.R;
import com.kemal.weatherapp.adapters.SlidingTabPagerAdapter;
import com.kemal.weatherapp.interfaces.OnWeatherItemDeleteListener;
import com.kemal.weatherapp.models.common.SlidingTabCityModel;
import com.kemal.weatherapp.models.geoname.GeoName;
import com.kemal.weatherapp.proxy.GeoNameRequest;
import com.kemal.weatherapp.proxy.GeoNameResponse;
import com.kemal.weatherapp.realm.RealmHelper;
import com.kemal.weatherapp.realm.UserSelectedCity;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseAppCompatActivity implements OnWeatherItemDeleteListener {

    private final String URL_GEONAME = "http://api.geonames.org/searchJSON?username=kemalkemal&featureClass=P&style=full&lang=tr&maxRows=7&name_equals=";

    @ViewById(R.id.main_activity_viewPager)
    ViewPager viewPager;

    @ViewById(R.id.main_activity_editTextSearch)
    EditText editTextSearchCity;

    @ViewById(R.id.main_activity_slidingTabLayout)
    PagerSlidingTabStrip pagerSlidingTabStrip;



    private SlidingTabPagerAdapter slidingTabPagerAdapter;

    private GeoNameResponse geoNameResponse;


    @AfterViews
    void init(){

        pagerSlidingTabStrip.setIndicatorColorResource(R.color.colorPrimary);
        initViewPager();

    }


    void initViewPager(){


        Realm realm = RealmHelper.getRealm(getApplicationContext());


        RealmResults<UserSelectedCity> result = realm.where(UserSelectedCity.class).findAll();

        ArrayList<SlidingTabCityModel> arrayList = new ArrayList<>();

        for (UserSelectedCity userSelectedCity:result){

            SlidingTabCityModel city = new SlidingTabCityModel();
            city.setCityName(userSelectedCity.getCityName());
            city.setWeatherQuery(userSelectedCity.getCityQueryName());
            arrayList.add(city);
        }





        if (arrayList.size() == 0){
            new MaterialDialog.Builder(this)
                    .title("Uyarı")
                    .content("Kayıtlı hiç şehriniz yok. Takip etmek istediğiniz şehri arama yaparak ekleyebilirsiniz.")
                    .positiveText("Tamam")
                    .show();
        }


        slidingTabPagerAdapter = new SlidingTabPagerAdapter(getSupportFragmentManager(), arrayList, this);
        viewPager.setAdapter(slidingTabPagerAdapter);

        pagerSlidingTabStrip.setViewPager(viewPager);
    }



    @Click(R.id.main_activity_imgSearch)
    void searchCity(){

        StartProgress();
        String search = editTextSearchCity.getText().toString();
        editTextSearchCity.setText("");
        VolleyManager volleyManager = getVolleyManager();

        GeoNameRequest request = new GeoNameRequest();
        volleyManager.setURL(URL_GEONAME + search);
        volleyManager.SendRequest(request, GeoNameResponse.class, new OnNetworkResponseListener() {

            @Override
            public void onResponseSuccess(NetworkBaseResponse response) {

                geoNameResponse = (GeoNameResponse) response;
                System.out.println(geoNameResponse);
                ShowAddCity();
                StopProgress();

            }

            @Override
            public void onResponseError(NetworkResponseErrorType errorType) {
                StopProgress();

            }
        });

    }


    private void ShowAddCity(){

        if (geoNameResponse.getTotalResultsCount() <= 0 ){
            new MaterialDialog.Builder(this)
                    .title("Uyarı")
                    .content("Aradaığınız kriterlere uygun kayıt bulunamadı.")
                    .positiveText("Tamam")
                    .show();
            return;
        }


        new MaterialDialog.Builder(this)
                .title("Seçiniz")
                .items(geoNameResponse.getGeonames())
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                        if (which>-1 && which < geoNameResponse.getGeonames().size()){
                            GeoName geoName = geoNameResponse.getGeonames().get(which);
                            AddCityToList(geoName);

                        }


                    }
                })
                .show();

    }

    private void AddCityToList(GeoName geoName){

        String queryName = geoName.getName()+","+geoName.getCountryName();
        String cityName = geoName.getName();


        Realm realm = RealmHelper.getRealm(getApplicationContext());

        RealmQuery<UserSelectedCity> query = realm.where(UserSelectedCity.class);
        query.equalTo("cityQueryName", queryName);
        RealmResults<UserSelectedCity> result = query.findAll();

        if (result.size()>0){

            new MaterialDialog.Builder(this)
                    .title("Uyarı")
                    .content(queryName+", daha once eklendi. ")
                    .positiveText("Tamam")
                    .show();

        }else{
            realm.beginTransaction();
            UserSelectedCity item =  realm.createObject(UserSelectedCity.class);
            item.setCityName(cityName);
            item.setCityQueryName(queryName);
            realm.commitTransaction();
            initViewPager();
        }





    }

    @Override
    public void onDelete(SlidingTabCityModel slidingTabCityModel) {

        Realm realm = RealmHelper.getRealm(getApplicationContext());

        realm.beginTransaction();

        RealmQuery<UserSelectedCity> query = realm.where(UserSelectedCity.class);
        query.equalTo("cityQueryName", slidingTabCityModel.getWeatherQuery());
        RealmResults<UserSelectedCity> result = query.findAll();
        result.deleteAllFromRealm();

        realm.commitTransaction();
        initViewPager();

    }
}
