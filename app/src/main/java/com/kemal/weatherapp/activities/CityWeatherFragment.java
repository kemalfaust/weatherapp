package com.kemal.weatherapp.activities;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.kemal.framework.base.BaseAppFragment;
import com.kemal.framework.network.NetworkBaseResponse;
import com.kemal.framework.network.enums.NetworkResponseErrorType;
import com.kemal.framework.network.interfaces.OnNetworkResponseListener;
import com.kemal.framework.volley.VolleyManager;
import com.kemal.weatherapp.R;
import com.kemal.weatherapp.adapters.GridAdapterWeatherDays;
import com.kemal.weatherapp.interfaces.OnWeatherItemDeleteListener;
import com.kemal.weatherapp.models.common.SlidingTabCityModel;
import com.kemal.weatherapp.models.weather.CurrentCondition;
import com.kemal.weatherapp.models.weather.Weather;
import com.kemal.weatherapp.models.weather.WeatherData;
import com.kemal.weatherapp.models.weather.WeatherDesc;
import com.kemal.weatherapp.proxy.GeoNameRequest;
import com.kemal.weatherapp.proxy.GeoNameResponse;
import com.kemal.weatherapp.proxy.WeatherRequest;
import com.kemal.weatherapp.proxy.WeatherResponse;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 * Created by kemal on 19/08/16.
 */
@EFragment(R.layout.city_weather_fragment)
public class CityWeatherFragment extends BaseAppFragment {

    SlidingTabCityModel tabCityModel;
    OnWeatherItemDeleteListener onWeatherItemDeleteListener;


    @ViewById(R.id.city_weather_fragment_currentTemp)
    TextView txtCurrentTemp;

    @ViewById(R.id.city_weather_fragment_currentImageView)
    ImageView currentImageView;

    @ViewById(R.id.city_weather_fragment_currentDescription)
    TextView txtDescription;


    @ViewById(R.id.city_weather_fragment_gridView)
    GridView gridViewWeather;


    @ViewById(R.id.city_weather_fragment_content)
    LinearLayout lnrContent;

    @ViewById(R.id.progressBar)
    ProgressBar progressBar;




    public static CityWeatherFragment_ newInstance(SlidingTabCityModel tabModel,OnWeatherItemDeleteListener onWeatherItemDeleteListener){
        CityWeatherFragment_ fragment = new CityWeatherFragment_();
        fragment.tabCityModel = tabModel;
        fragment.onWeatherItemDeleteListener = onWeatherItemDeleteListener;
        return fragment;
    }

    @AfterViews
    void init(){

        lnrContent.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        VolleyManager volleyManager = VolleyManager.getInstance(getContext());

        String weatherURL = "http://api.worldweatheronline.com/premium/v1/weather.ashx?key=3ad785e564f54b85a8e153500161908" +
                "&format=json" +
                "&num_of_days=5" +
                "&q="+tabCityModel.getWeatherQuery();

        WeatherRequest request = new WeatherRequest();
        volleyManager.setURL(weatherURL);
        volleyManager.SendRequest(request, WeatherResponse.class, new OnNetworkResponseListener() {

            @Override
            public void onResponseSuccess(NetworkBaseResponse response) {

                WeatherResponse weatherResponse = (WeatherResponse) response;
                updateWeatherUI(weatherResponse);

            }

            @Override
            public void onResponseError(NetworkResponseErrorType errorType) {

            }
        });
    }

    void updateWeatherUI(WeatherResponse weatherRespons){

        if (weatherRespons.getData() == null)
            return;

        WeatherData weatherData = weatherRespons.getData();

        if (weatherData.getCurrent_condition() == null || weatherData.getCurrent_condition().size() <=0 )
            return;

        lnrContent.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        CurrentCondition currentCondition = weatherData.getCurrent_condition().get(0);


        String currentTemp = currentCondition.getTemp_C();

        if (currentCondition.getWeatherDesc() != null && currentCondition.getWeatherDesc().size()>0){

            String strDesc = "";
            for (WeatherDesc desc:currentCondition.getWeatherDesc()){
                if (desc.getValue() != null)
                    strDesc+=desc.getValue()+"\n";
            }
            txtDescription.setText(strDesc);

        }

        txtCurrentTemp.setText(currentTemp);

        if (weatherData.getWeather() != null && weatherData.getWeather().size()>0){

            ArrayList<Weather> weathers = weatherData.getWeather();
            GridAdapterWeatherDays gridAdapterWeatherDays = new GridAdapterWeatherDays(weathers, getContext());
            gridViewWeather.setAdapter(gridAdapterWeatherDays);

        }

        if (currentCondition.getWeatherIconUrl() != null && currentCondition.getWeatherIconUrl().size()>0){
            String weatherIconUrl = currentCondition.getWeatherIconUrl().get(0).getValue();
            if (weatherIconUrl != null && !weatherIconUrl.equalsIgnoreCase(""))
                Picasso.with(getContext()).load(weatherIconUrl).into(currentImageView);
        }



    }

    @Click(R.id.city_weather_fragment_currentDelete)
    void deleteCurrentCity(){

        new MaterialDialog.Builder(getContext())
                .title("Uyarı")
                .content(tabCityModel.getCityName()+" silinsin mi ?")
                .positiveText("Tamam")
                .negativeText("İptal")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (onWeatherItemDeleteListener != null){
                            onWeatherItemDeleteListener.onDelete(tabCityModel);
                        }
                    }
                })
                .show();


    }




}
