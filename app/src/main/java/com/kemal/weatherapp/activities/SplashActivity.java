package com.kemal.weatherapp.activities;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.kemal.framework.base.BaseAppCompatActivity;
import com.kemal.weatherapp.R;
import com.kemal.weatherapp.realm.RealmHelper;
import com.kemal.weatherapp.realm.UserSelectedCity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by kemal on 20/08/16.
 */
@EActivity(R.layout.splash_activity)
public class SplashActivity extends BaseAppCompatActivity {

    final String PREF_KEY = "weatherapp";

    Handler handler = new Handler();
    @AfterViews
    void init(){


        handler.post(new Runnable() {
            @Override
            public void run() {
                SharedPreferences preferences = getSharedPreferences(PREF_KEY, MODE_PRIVATE);

                boolean isInited = preferences.getBoolean("isInited",false);
                if (isInited){
                    goMain();
                }else {
                    initRealm();
                }
            }
        });





    }


    void initRealm(){
        Realm realm = RealmHelper.getRealm(getApplicationContext());

        RealmResults<UserSelectedCity> result = realm.where(UserSelectedCity.class).findAll();

        if (result.size() == 0){
            realm.beginTransaction();
            UserSelectedCity item =  realm.createObject(UserSelectedCity.class);
            item.setCityName("İstanbul");
            item.setCityQueryName("İstanbul,Türkiye");
            realm.commitTransaction();
        }
        SharedPreferences.Editor editor = getSharedPreferences(PREF_KEY, MODE_PRIVATE).edit();
        editor.putBoolean("isInited", true);
        editor.commit();
        goMain();

    }

    void goMain(){
        StartActivity(MainActivity_.class);
        FinishActivity();
    }



}
