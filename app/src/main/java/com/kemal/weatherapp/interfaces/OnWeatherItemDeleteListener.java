package com.kemal.weatherapp.interfaces;

import com.kemal.weatherapp.models.common.SlidingTabCityModel;

/**
 * Created by kemal on 20/08/16.
 */
public interface OnWeatherItemDeleteListener {
    void onDelete(SlidingTabCityModel slidingTabCityModel);
}
