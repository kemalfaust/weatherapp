package com.kemal.weatherapp.proxy;

import com.kemal.framework.network.NetworkBaseResponse;
import com.kemal.weatherapp.models.weather.WeatherData;

/**
 * Created by kemal on 19/08/16.
 */
public class WeatherResponse extends NetworkBaseResponse {

    private WeatherData data;

    public WeatherResponse() {

    }

    public WeatherData getData() {
        return data;
    }

    public void setData(WeatherData data) {
        this.data = data;
    }
}
