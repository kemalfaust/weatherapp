package com.kemal.weatherapp.proxy;

import com.kemal.framework.network.NetworkBaseResponse;
import com.kemal.weatherapp.models.geoname.GeoName;

import java.util.ArrayList;

/**
 * Created by kemal on 19/08/16.
 */
public class GeoNameResponse extends NetworkBaseResponse {

    private int totalResultsCount;
    private ArrayList<GeoName> geonames;

    public GeoNameResponse() {

    }

    public int getTotalResultsCount() {
        return totalResultsCount;
    }

    public void setTotalResultsCount(int totalResultsCount) {
        this.totalResultsCount = totalResultsCount;
    }

    public ArrayList<GeoName> getGeonames() {
        return geonames;
    }

    public void setGeonames(ArrayList<GeoName> geonames) {
        this.geonames = geonames;
    }
}
