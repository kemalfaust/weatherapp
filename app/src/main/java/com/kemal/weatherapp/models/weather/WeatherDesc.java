package com.kemal.weatherapp.models.weather;

import com.kemal.framework.network.NetworkBaseModel;

/**
 * Created by kemal on 19/08/16.
 */
public class WeatherDesc extends NetworkBaseModel {


    private String value;


    public WeatherDesc() {

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
