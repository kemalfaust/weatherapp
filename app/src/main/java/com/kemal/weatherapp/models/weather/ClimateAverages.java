package com.kemal.weatherapp.models.weather;

import com.kemal.framework.network.NetworkBaseModel;

import java.util.ArrayList;

/**
 * Created by kemal on 19/08/16.
 */
public class ClimateAverages extends NetworkBaseModel {

    private ArrayList<WeatherMonth> month;

    public ClimateAverages() {

    }

    public ArrayList<WeatherMonth> getMonth() {
        return month;
    }

    public void setMonth(ArrayList<WeatherMonth> month) {
        this.month = month;
    }
}
