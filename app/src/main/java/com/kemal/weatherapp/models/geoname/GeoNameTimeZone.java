package com.kemal.weatherapp.models.geoname;

import com.kemal.framework.network.NetworkBaseModel;

/**
 * Created by kemal on 19/08/16.
 */
public class GeoNameTimeZone extends NetworkBaseModel {

    private int gmtOffset;
    private String timeZoneId;
    private int dstOffset;

    public GeoNameTimeZone() {

    }

    public int getGmtOffset() {
        return gmtOffset;
    }

    public void setGmtOffset(int gmtOffset) {
        this.gmtOffset = gmtOffset;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public int getDstOffset() {
        return dstOffset;
    }

    public void setDstOffset(int dstOffset) {
        this.dstOffset = dstOffset;
    }
}
