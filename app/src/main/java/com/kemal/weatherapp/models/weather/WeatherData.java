package com.kemal.weatherapp.models.weather;

import com.kemal.framework.network.NetworkBaseModel;

import java.util.ArrayList;

/**
 * Created by kemal on 19/08/16.
 */
public class WeatherData extends NetworkBaseModel {


    private ArrayList<CurrentCondition> current_condition;
    private ArrayList<Weather> weather;
    private ArrayList<ClimateAverages> ClimateAverages;

    public WeatherData() {

    }

    public ArrayList<CurrentCondition> getCurrent_condition() {
        return current_condition;
    }

    public void setCurrent_condition(ArrayList<CurrentCondition> current_condition) {
        this.current_condition = current_condition;
    }

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public ArrayList<com.kemal.weatherapp.models.weather.ClimateAverages> getClimateAverages() {
        return ClimateAverages;
    }

    public void setClimateAverages(ArrayList<com.kemal.weatherapp.models.weather.ClimateAverages> climateAverages) {
        ClimateAverages = climateAverages;
    }
}
