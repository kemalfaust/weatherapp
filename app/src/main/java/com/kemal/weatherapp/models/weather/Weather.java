package com.kemal.weatherapp.models.weather;

import com.kemal.framework.network.NetworkBaseModel;

import java.util.ArrayList;

/**
 * Created by kemal on 19/08/16.
 */
public class Weather extends NetworkBaseModel {

    private String date;
    private ArrayList<Astronomy> astronomy;
    private String maxtempC;
    private String maxtempF;
    private String mintempC;
    private String mintempF;
    private String uvIndex;
    private ArrayList<WeatherHourly> hourly;

    public Weather() {

    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<Astronomy> getAstronomy() {
        return astronomy;
    }

    public void setAstronomy(ArrayList<Astronomy> astronomy) {
        this.astronomy = astronomy;
    }

    public String getMaxtempC() {
        return maxtempC;
    }

    public void setMaxtempC(String maxtempC) {
        this.maxtempC = maxtempC;
    }

    public String getMaxtempF() {
        return maxtempF;
    }

    public void setMaxtempF(String maxtempF) {
        this.maxtempF = maxtempF;
    }

    public String getMintempC() {
        return mintempC;
    }

    public void setMintempC(String mintempC) {
        this.mintempC = mintempC;
    }

    public String getMintempF() {
        return mintempF;
    }

    public void setMintempF(String mintempF) {
        this.mintempF = mintempF;
    }

    public String getUvIndex() {
        return uvIndex;
    }

    public void setUvIndex(String uvIndex) {
        this.uvIndex = uvIndex;
    }

    public ArrayList<WeatherHourly> getHourly() {
        return hourly;
    }

    public void setHourly(ArrayList<WeatherHourly> hourly) {
        this.hourly = hourly;
    }
}
