package com.kemal.weatherapp.models.geoname;

import com.kemal.framework.network.NetworkBaseModel;

/**
 * Created by kemal on 19/08/16.
 */
public class GeoNameBbox extends NetworkBaseModel {

    private double east;
    private double south;
    private double north;
    private double west;

    public GeoNameBbox() {

    }

    public double getEast() {
        return east;
    }

    public void setEast(double east) {
        this.east = east;
    }

    public double getSouth() {
        return south;
    }

    public void setSouth(double south) {
        this.south = south;
    }

    public double getNorth() {
        return north;
    }

    public void setNorth(double north) {
        this.north = north;
    }

    public double getWest() {
        return west;
    }

    public void setWest(double west) {
        this.west = west;
    }
}
