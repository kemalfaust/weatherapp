package com.kemal.weatherapp.models.common;

/**
 * Created by kemal on 19/08/16.
 */
public class SlidingTabCityModel {

    private String cityName;
    private String weatherQuery;

    public SlidingTabCityModel() {

    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getWeatherQuery() {
        return weatherQuery;
    }

    public void setWeatherQuery(String weatherQuery) {
        this.weatherQuery = weatherQuery;
    }

    @Override
    public String toString() {
        return this.cityName;
    }
}
