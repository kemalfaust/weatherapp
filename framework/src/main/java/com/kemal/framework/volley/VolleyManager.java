package com.kemal.framework.volley;


import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kemal.framework.network.NetworkBaseRequest;
import com.kemal.framework.network.NetworkBaseResponse;
import com.kemal.framework.network.enums.NetworkResponseErrorType;
import com.kemal.framework.network.interfaces.OnNetworkResponseListener;
import com.kemal.framework.utils.JsonUtility;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kemal on 19/08/16.
 */
public class VolleyManager {

    private String URL ;

    private static VolleyManager ourInstance = new VolleyManager();
    private static Context context;
    private RequestQueue requestQueue;

    public static VolleyManager getInstance(Context context) {
        VolleyManager.context = context;
        return ourInstance;
    }

    private VolleyManager() {

    }

    private RequestQueue getRequestQueue() {

        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }


        return requestQueue;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getURL() {
        return URL;
    }

    public void SendRequest(final NetworkBaseRequest request, final Class<? extends NetworkBaseResponse> responseCls, final OnNetworkResponseListener onNetworkResponseListener) {


        final String jsonBody = JsonUtility.getJSONStringFromObject(request);


        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL , new Response.Listener<String>() {
            @Override
            public void onResponse(String responseData) {

                NetworkBaseResponse response = null;
                try {
                    response = (NetworkBaseResponse) JsonUtility.getObjectFromJSONString(responseCls, responseData);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (onNetworkResponseListener != null){

                    if (response == null)
                        onNetworkResponseListener.onResponseError(NetworkResponseErrorType.NETWORK_ERROR);
                    else
                        onNetworkResponseListener.onResponseSuccess(response);

                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (onNetworkResponseListener != null)
                    onNetworkResponseListener.onResponseError(NetworkResponseErrorType.NETWORK_ERROR);

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return jsonBody.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headersMap = new HashMap<String, String>();
                headersMap.put("Content-Type", "application/json");

                return headersMap;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {

                Map<String, String> headers = response.headers;

                for (Map.Entry<String, String> headerItem : headers.entrySet()) {
                    String k = headerItem.getKey();
                    String v = headerItem.getValue();

                    System.out.println(" k : v = [" + k + "] : [" + v + "]");

                }

                return super.parseNetworkResponse(response);
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                60 * 1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue queue = getRequestQueue();
        queue.add(stringRequest);

    }



}
