package com.kemal.framework.utils;

import android.content.Intent;
import android.os.Bundle;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by kemal on 19/08/16.
 */
public class JsonUtility {

    private static final String KEY_INTENT_OBJECT="_object";
    private static final String KEY_INTENT_CLASS="_class";
    private static final ObjectMapper objectMapper = new ObjectMapper();


    public static void addObjectToIntent(Intent intent, Object object) throws JsonProcessingException {

        String clsName = object.getClass().getName();
        String objJsonString = JsonUtility.getJSONStringFromObject(object);
        intent.putExtra(KEY_INTENT_OBJECT, objJsonString);
        intent.putExtra(KEY_INTENT_CLASS, clsName);
    }

    /*
    public static Object getObjectFromIntent(Intent intent){

        if (intent == null)
            return null;

        Object intentModel = null;
        Bundle bundle = intent.getExtras();

        if(bundle != null){

            String strJson = bundle.getString(KEY_INTENT_OBJECT);
            String clsName = bundle.getString(KEY_INTENT_CLASS);

            if(strJson != null && !strJson.equalsIgnoreCase("")){
                Class<?> cls = Object.class;
                if(clsName != null && !clsName.equalsIgnoreCase("")){
                    try {
                        cls = Class.forName(clsName);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }

                intentModel = JsonUtility.getObjectFromJSONString(cls, strJson);
            }

        }
        return intentModel;
    }*/

    public static String getJSONStringFromObject(Object object)  {

//        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.PUBLIC_ONLY);
//        objectMapper.setVisibility(PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
//        objectMapper.setVisibility(PropertyAccessor.SETTER, JsonAutoDetect.Visibility.NONE);

        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {

            return "";
        }

    }

    public static Object getObjectFromJSONString(Class<?> cls, String jsonString) throws IOException {


//        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.PUBLIC_ONLY);
//        objectMapper.setVisibility(PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
//        objectMapper.setVisibility(PropertyAccessor.SETTER, JsonAutoDetect.Visibility.NONE);

        return objectMapper.readValue(jsonString, cls);

    }

}
