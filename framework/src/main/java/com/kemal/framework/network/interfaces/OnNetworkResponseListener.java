package com.kemal.framework.network.interfaces;

import com.kemal.framework.network.NetworkBaseResponse;
import com.kemal.framework.network.enums.NetworkResponseErrorType;

/**
 * Created by kemal on 19/08/16.
 */
public interface OnNetworkResponseListener {

    void onResponseSuccess(NetworkBaseResponse response);
    void onResponseError(NetworkResponseErrorType errorType);
}
