package com.kemal.framework.network;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by kemal on 19/08/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NetworkBaseResponse {
}
