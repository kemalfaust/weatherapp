package com.kemal.framework.base;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.Window;

import com.kemal.framework.base.interfaces.OnICanceledListener;
import com.kemal.framework.base.interfaces.OnIInformedListener;


public abstract class IDialogFragment extends DialogFragment {


	private Dialog iDialog = null;

	private OnICanceledListener onICanceledListener = null;
	private OnIInformedListener onIInformedListener = null;

	
	private boolean isCancelable = false;
	private boolean isCancelOnTouchOutSide = false;
	
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		iDialog = super.onCreateDialog(savedInstanceState);
		iDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		iDialog.setCancelable(isCancelable);
		iDialog.setCanceledOnTouchOutside(isCancelOnTouchOutSide);
		
		iDialog.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
				if(onICanceledListener != null)
					onICanceledListener.onCancelled();
				
			}
		});
		
		iDialog.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				if(onICanceledListener != null)
					onICanceledListener.onCancelled();
				
			}
		});
		
		iDialog.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				
				if(keyCode == KeyEvent.KEYCODE_BACK){
					if(onICanceledListener != null){
						onICanceledListener.onCancelled();
						return false;
					}
					
				}
				
				
				return true;
			}
		});

		
		
		
		return iDialog;
	}


	
	public void setOnICanceledListener(OnICanceledListener onICanceledListener) {
		this.onICanceledListener = onICanceledListener;
	}
	
	public void setOnIInformedListener(OnIInformedListener onIInformedListener) {
		this.onIInformedListener = onIInformedListener;
	}

}
