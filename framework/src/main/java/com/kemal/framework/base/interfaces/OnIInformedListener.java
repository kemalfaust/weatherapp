package com.kemal.framework.base.interfaces;

public interface OnIInformedListener {

	public void onInformed();
}
