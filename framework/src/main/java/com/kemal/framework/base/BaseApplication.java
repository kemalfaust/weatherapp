package com.kemal.framework.base;

import android.app.Application;

/**
 * Created by kemal on 19/08/16.
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
