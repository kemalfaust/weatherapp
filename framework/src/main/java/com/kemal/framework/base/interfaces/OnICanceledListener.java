package com.kemal.framework.base.interfaces;

public interface OnICanceledListener {
	public void onCancelled();
}
