package com.kemal.framework.base;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kemal.framework.R;
import com.kemal.framework.utils.JsonUtility;
import com.kemal.framework.volley.VolleyManager;

/**
 * Created by kemal on 19/08/16.
 */
public abstract class BaseAppCompatActivity extends AppCompatActivity {

    public VolleyManager getVolleyManager() {
        return VolleyManager.getInstance(getApplicationContext());
    }


    public void StartActivity(Intent intent){
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public void StartActivity(Class<?> target){
        StartActivity(target, null);
    }
    public void StartActivity(Class<?> target,Object model){

        Intent intent = new Intent(this, target);
        if (model != null)
            try {
                JsonUtility.addObjectToIntent(intent, model);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        // overridePendingTransition(R.anim.activity_start, R.anim.activity_start);
    }

    public void FinishActivity(){
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onBackPressed() {

        //endAnimation();
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        //overridePendingTransition(R.anim.activity_end, R.anim.activity_end);
    }

    private IProgressDialog iProgressDialog = null;

    public void StartProgress(){

        FragmentManager fragmentManager = getSupportFragmentManager();
        iProgressDialog = new IProgressDialog();
        iProgressDialog.show(fragmentManager, IProgressDialog.TAG);

    }

    public void StopProgress() {
        try{

            if (iProgressDialog != null) {
                iProgressDialog.dismiss();
            }

        }catch(Exception e){
            e.printStackTrace();
        }

    }

}
