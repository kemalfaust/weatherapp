package com.kemal.framework.base;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.kemal.framework.R;


public class IProgressDialog extends IDialogFragment {
	
	public static final String TAG ="IProgressDialog";
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
       // dialog.getWindow().setWindowAnimations(R.style.dialog_animation_fade);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().getAttributes().dimAmount = 0.55f;

        return dialog;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.progress_dialog, null);
		//view.setBackgroundColor(Color.TRANSPARENT);
		return view;
	}

}
